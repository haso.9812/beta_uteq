using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unload : MonoBehaviour
{
    public int numeroescena;
    bool unloaded;

    private void OnTriggerEnter(Collider other)
    {
        if (!unloaded)
        {
            unloaded = true;
            CambioEscenas.cambioEscenas.UnloadScene(numeroescena);
        }
    }
}
