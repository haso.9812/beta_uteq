using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class CambioEscenas : MonoBehaviour
{
    public static CambioEscenas cambioEscenas;
    bool gameStart;
    [SerializeField] private GameObject dialogueMark;
    [SerializeField] private GameObject dialoguePanel;
    [SerializeField] private TMP_Text dialogueText;

    public GameObject getDialogueMark()
    {
        return dialogueMark;
    }
    public GameObject getDialoguePanel()
    {
        return dialoguePanel;
    }
    public TMP_Text getDialogueText()
    {
        return dialogueText;
    }

    void Awake()
    {
        if (!gameStart)
        {
            cambioEscenas = this;
            SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
            gameStart = true;
        }
    }

   public void UnloadScene(int scene)
    {
        StartCoroutine(Unload(scene));
    }

   IEnumerator Unload(int scene)
    {
        yield return null;
        SceneManager.UnloadSceneAsync(scene);
    }
}
