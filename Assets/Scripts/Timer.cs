using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class Timer : MonoBehaviour
{
    [SerializeField]
    private GameObject GameOverPanel;
    float currentTime = 0;
    float startingTime = 200;

    [SerializeField]
    private TMP_Text TimerText;
    [SerializeField]
    private Text TextoPanel;
    private void Start()
    {
        Time.timeScale = 1f;
        currentTime = startingTime;
    }

    private void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        TimerText.text = currentTime.ToString("000");


        if(currentTime <= 3)
        {
            TimerText.color = Color.red;
            if(currentTime <= 0)
            {
                TimerText.color = Color.red;
                currentTime = 00;
                GameOverPanel.SetActive(true);
                TextoPanel.text = "Se acab� el tiempo";
                Time.timeScale = 0f;
            }
        }
    }
    public void Restart()
    {
        System.Console.Write("Me est�n picando");
        SceneManager.LoadScene(0);
    }
}
