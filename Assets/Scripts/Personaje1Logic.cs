using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class Personaje1Logic : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;
    private Animator anim;
    public float x, y;
    public int monedas;
    [SerializeField] TMP_Text texto;
    [SerializeField] private GameObject GameOverPanel;
    [SerializeField] private Text TextoPanel;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>(); 
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);

        anim.SetFloat("velX", x);
        anim.SetFloat("velY", y);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            monedas++;
            texto.text = monedas.ToString() + "/10";
            Destroy(other.gameObject);
            Debug.Log("Monedas: " + monedas);
            if (monedas == 10)
            {
                GameOverPanel.SetActive(true);
                TextoPanel.text = "ˇFelicidades!";
                Time.timeScale = 0f;
            }
        }
    }
}
