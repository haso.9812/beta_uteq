using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Load : MonoBehaviour
{
    public Animator transition;
    public int numeroEscena;
    public int eliminarEscena;
    public float transitionTime = 1f;
    bool loaded;

    private void OnTriggerEnter(Collider other)
    {
        if (!loaded)
        {
            if (other.tag == "Player")
            {
                LoadNextLevel();
            }
        }

    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(numeroEscena));
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadSceneAsync(numeroEscena, LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync(eliminarEscena);
        
        loaded = true;
    }
}
